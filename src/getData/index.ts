import * as Utils from '/opt/nodejs'

export const handler = async (event: any = {}): Promise<any> => {
  console.log('-----1-----')
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(await Utils.getProperties())
  };
}
