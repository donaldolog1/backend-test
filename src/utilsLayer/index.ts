import axios from "axios";

const URL_ITEMS = "https://se04exe6ve.execute-api.us-east-2.amazonaws.com/luis_lopez_dev/items"

export const getProperties = async function() {
    console.log(`-------getProperties: ${URL_ITEMS}---------`)
    const { data } = await axios.get(URL_ITEMS)
    console.log("-------getProperties: End---------")
    return data
}
