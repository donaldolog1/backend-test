#!/bin/bash

set -e

for i in src/*Layer; do
  echo transpiling $i function
  (cd $i; npm install; npm run build;)
done

for i in src/*; do
  if [[ $i =~ (.*)Layer$ ]]
    then
    echo No transpiling $i function
    else
    echo transpiling $i function
    (cd $i; npm install; npm run build)
  fi
done
